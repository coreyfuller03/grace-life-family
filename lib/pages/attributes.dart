import 'package:flutter/material.dart';

import '../cards.dart';
import '../flash_card.dart';
import '../globals.dart' as globals;
import '../nav.dart';

class AttributesWidget extends StatefulWidget {
  const AttributesWidget({super.key});

  @override
  AttributesWidgetState createState() => AttributesWidgetState();
}

class AttributesWidgetState extends State<AttributesWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _loadAttributesOfGod(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          Container flashCardBody;
          if (snapshot.hasData) {
            flashCardBody = Container(
              color: Colors.grey.shade100,
              child: Center(
                child: Cards(
                  type: globals.FlashCardTypes.attributes,
                  flashCards: snapshot.data,
                ),
              ),
            );
          } else {
            flashCardBody = Container(
              color: Colors.grey.shade100,
              child: Center(
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    'Loading flash cards...',
                    style: TextStyle(
                      fontSize: globals.smallFont,
                    ),
                  ),
                ),
              ),
            );
          }
          return GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Scaffold(
              drawer: const NavDrawer(),
              resizeToAvoidBottomInset: true,
              appBar: AppBar(
                iconTheme: IconThemeData(
                  size: globals.largeFont,
                  color: Colors.grey.shade100,
                ),
                title: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    'GOD IS',
                    style: TextStyle(
                      fontFamily: 'BigNeo',
                      fontSize: globals.xLargeFont,
                    ),
                  ),
                ),
                actions: <Widget>[
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: IconButton(
                      alignment: Alignment.center,
                      color: Colors.grey.shade100,
                      iconSize: globals.largeFont,
                      icon: const Icon(Icons.help_outline),
                      onPressed: () => showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          backgroundColor: Colors.grey.shade100,
                          title: const Text('God Is Cards'),
                          content: const Text(
                              'We are always learning more about who God is! Tap the card to flip it over. Swipe right or left to get to a different card.'),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'OK'),
                              style: TextButton.styleFrom(
                                foregroundColor: Colors.grey.shade900,
                              ),
                              child: const Text('OK'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: flashCardBody,
            ),
          );
        });
  }

  static Future<List<FlashCard>> _loadAttributesOfGod(
      BuildContext context) async {
    if (globals.attributesOfGodCards.isNotEmpty) {
      return globals.attributesOfGodCards;
    }
    List<FlashCard> attributesOfGodCards = [];
    if (globals.attributesCards.isNotEmpty) {
      for (int i = 0; i < globals.attributesCards.length - 1; i++) {
        attributesOfGodCards.add(
          FlashCard(
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: globals.attributesCards[i].front,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: globals.attributesCards[i].back,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            globals.attributesCards[i].color,
          ),
        );
      }
      globals.attributesOfGodCards = attributesOfGodCards;
    }
    return attributesOfGodCards;
  }
}
