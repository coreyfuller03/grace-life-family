import 'package:flutter/material.dart';

import '../globals.dart' as globals;
import '../nav.dart';

class Books extends StatelessWidget {
  const Books({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        iconTheme: IconThemeData(
          size: globals.largeFont,
          color: Colors.grey.shade100,
        ),
        title: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            'BOOKS OF THE BIBLE',
                    style: TextStyle(
                      fontFamily: 'BigNeo',
                      fontSize: globals.largeFont,
                    ),
          ),
        ),
      ),
      body: BibleBooksGrouped(),
    );
  }
}

class BibleBooksGrouped extends StatelessWidget {
  final List<Map<String, dynamic>> bibleBooks = [
    {
      'authorship': 'Who wrote the Bible? Men inspired by God!',
    },
    {
      'Old Testament': [
        {
          'Law': [
            {
              'Genesis': {'Chapters': '50', 'Author': 'Moses'}
            },
            {
              'Exodus': {'Chapters': '40', 'Author': 'Moses'}
            },
            {
              'Leviticus': {'Chapters': '27', 'Author': 'Moses'}
            },
            {
              'Numbers': {'Chapters': '36', 'Author': 'Moses'}
            },
            {
              'Deuteronomy': {'Chapters': '34', 'Author': 'Moses'}
            },
          ],
        },
        {
          'History': [
            {
              'Joshua': {'Chapters': '24', 'Author': 'Unknown'}
            },
            {
              'Judges': {'Chapters': '21', 'Author': 'Unknown'}
            },
            {
              'Ruth': {'Chapters': '4', 'Author': 'Unknown'}
            },
            {
              '1 Samuel': {'Chapters': '31', 'Author': 'Unknown'}
            },
            {
              '2 Samuel': {'Chapters': '24', 'Author': 'Unknown'}
            },
            {
              '1 Kings': {'Chapters': '22', 'Author': 'Unknown'}
            },
            {
              '2 Kings': {'Chapters': '25', 'Author': 'Unknown'}
            },
            {
              '1 Chronicles': {
                'Chapters': '29',
                'Author': 'Unknown (Possibly Ezra)'
              }
            },
            {
              '2 Chronicles': {
                'Chapters': '36',
                'Author': 'Unknown (Possibly Ezra)'
              }
            },
            {
              'Ezra': {'Chapters': '10', 'Author': 'Unknown (Possibly Ezra)'}
            },
            {
              'Nehemiah': {
                'Chapters': '13',
                'Author': 'Unknown (Possibly Ezra or Nehemiah)'
              }
            },
            {
              'Esther': {'Chapters': '10', 'Author': 'Unknown'}
            },
          ],
        },
        {
          'Poetry': [
            {
              'Job': {'Chapters': '42', 'Author': 'Unknown'}
            },
            {
              'Psalms': {
                'Chapters': '150',
                'Author':
                    'Various (David, Asaph, the sons of Korah, Heman, Ethan, Moses, Solomon, etc.)'
              }
            },
            {
              'Proverbs': {
                'Chapters': '31',
                'Author': 'Various (Solomon, Agur, and Lemuel)'
              }
            },
            {
              'Ecclesiastes': {
                'Chapters': '12',
                'Author': 'Unknown (Possibly Solomon)'
              }
            },
            {
              'Song of Solomon': {
                'Chapters': '8',
                'Author': 'Unknown (Possibly Solomon)'
              }
            },
          ],
        },
        {
          'Major Prophets': [
            {
              'Isaiah': {'Chapters': '66', 'Author': 'Isaiah'}
            },
            {
              'Jeremiah': {'Chapters': '52', 'Author': 'Jeremiah'}
            },
            {
              'Lamentations': {
                'Chapters': '5',
                'Author': 'Unknown (Possibly Jeremiah)'
              }
            },
            {
              'Ezekiel': {
                'Chapters': '48',
                'Author': 'Unknown (Possibly Ezekiel)'
              }
            },
            {
              'Daniel': {
                'Chapters': '12',
                'Author': 'Unknown (Possibly Daniel)'
              }
            },
          ],
        },
        {
          'Minor Prophets': [
            {
              'Hosea': {'Chapters': '14', 'Author': 'Hosea'}
            },
            {
              'Joel': {'Chapters': '3', 'Author': 'Joel'}
            },
            {
              'Amos': {'Chapters': '9', 'Author': 'Amos'}
            },
            {
              'Obadiah': {
                'Chapters': '1',
                'Author': 'Unknown (Possibly Obadiah)'
              }
            },
            {
              'Jonah': {'Chapters': '4', 'Author': 'Jonah'}
            },
            {
              'Micah': {'Chapters': '7', 'Author': 'Micah'}
            },
            {
              'Nahum': {'Chapters': '3', 'Author': 'Nahum'}
            },
            {
              'Habakkuk': {'Chapters': '3', 'Author': 'Habakkuk'}
            },
            {
              'Zephaniah': {'Chapters': '3', 'Author': 'Zephaniah'}
            },
            {
              'Haggai': {'Chapters': '2', 'Author': 'Haggai'}
            },
            {
              'Zechariah': {'Chapters': '14', 'Author': 'Zechariah'}
            },
            {
              'Malachi': {'Chapters': '4', 'Author': 'Malachi'}
            },
          ],
        },
      ],
    },
    {
      'New Testament': [
        {
          'Gospels': [
            {
              'Matthew': {'Chapters': '28', 'Author': 'Matthew'}
            },
            {
              'Mark': {'Chapters': '16', 'Author': 'Mark (John Mark)'}
            },
            {
              'Luke': {'Chapters': '24', 'Author': 'Luke'}
            },
            {
              'John': {'Chapters': '21', 'Author': 'John'}
            },
          ],
        },
        {
          'History': [
            {
              'Acts': {'Chapters': '28', 'Author': 'Luke'}
            },
          ],
        },
        {
          'Paul\'s Letters': [
            {
              'Romans': {'Chapters': '16', 'Author': 'Paul'}
            },
            {
              '1 Corinthians': {'Chapters': '16', 'Author': 'Paul'}
            },
            {
              '2 Corinthians': {'Chapters': '13', 'Author': 'Paul'}
            },
            {
              'Galatians': {'Chapters': '6', 'Author': 'Paul'}
            },
            {
              'Ephesians': {'Chapters': '6', 'Author': 'Paul'}
            },
            {
              'Philippians': {'Chapters': '4', 'Author': 'Paul'}
            },
            {
              'Colossians': {'Chapters': '4', 'Author': 'Paul'}
            },
            {
              '1 Thessalonians': {'Chapters': '5', 'Author': 'Paul'}
            },
            {
              '2 Thessalonians': {'Chapters': '3', 'Author': 'Paul'}
            },
            {
              '1 Timothy': {'Chapters': '6', 'Author': 'Paul'}
            },
            {
              '2 Timothy': {'Chapters': '4', 'Author': 'Paul'}
            },
            {
              'Titus': {'Chapters': '3', 'Author': 'Paul'}
            },
            {
              'Philemon': {'Chapters': '1', 'Author': 'Paul'}
            },
          ],
        },
        {
          'General Letters': [
            {
              'Hebrews': {
                'Chapters': '13',
                'Author': 'Unknown (Possibly Paul, Barnabas, or Apollos)'
              }
            },
            {
              'James': {'Chapters': '5', 'Author': 'James (Brother of Jesus)'}
            },
            {
              '1 Peter': {'Chapters': '5', 'Author': 'Peter'}
            },
            {
              '2 Peter': {'Chapters': '3', 'Author': 'Peter'}
            },
            {
              '1 John': {'Chapters': '5', 'Author': 'John'}
            },
            {
              '2 John': {'Chapters': '1', 'Author': 'John'}
            },
            {
              '3 John': {'Chapters': '1', 'Author': 'John'}
            },
            {
              'Jude': {'Chapters': '1', 'Author': 'Jude (Brother of James)'}
            },
          ],
        },
        {
          'Prophecy': [
            {
              'Revelation': {'Chapters': '22', 'Author': 'John'}
            },
          ],
        },
      ],
    },
  ];

  BibleBooksGrouped({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: bibleBooks.length,
      itemBuilder: (context, index) {
        final group = bibleBooks[index];
        final title = group.keys.first;

        if (title == 'authorship') {
          return Center(
            child: Padding(
              padding: EdgeInsets.all(globals.smallPadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      'Who wrote the Bible?',
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: globals.mediumFont,
                      ),
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      'Men inspired by God!',
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: globals.smallFont,
                      ),
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      '2 Peter 1:20-21',
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: globals.smallFont,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          final subGroups = group[title]!;
          return BookGroup(title: title, subGroups: subGroups);
        }
      },
    );
  }
}

class BookGroup extends StatelessWidget {
  final String title;
  final List<Map<String, dynamic>> subGroups;

  const BookGroup({super.key, required this.title, required this.subGroups});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              title,
              style: TextStyle(
                fontSize: globals.xLargeFont,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        for (final subGroup in subGroups)
          for (final subType in subGroup.keys)
            BookSubGroup(title: subType, books: subGroup[subType]!),
        const Divider(),
      ],
    );
  }
}

class BookSubGroup extends StatelessWidget {
  final String title;
  final List<Map<String, Map<String, String>>> books;

  const BookSubGroup({super.key, required this.title, required this.books});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            title,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        for (final book in books)
          BookCard(
            bookName: book.keys.first,
            details: book.values.first,
          ),
      ],
    );
  }
}

class BookCard extends StatelessWidget {
  final String bookName;
  final Map<String, String> details;

  const BookCard({
    super.key,
    required this.bookName,
    required this.details,
  });

  @override
  Widget build(BuildContext context) {
    final String chapters = details['Chapters'] ?? '';
    final String author = details['Author'] ?? '';

    return Center(
      child: SizedBox(
        width: globals.screenWidth * 2 / 3,
        child: Card(
          color: Theme.of(context).primaryColorLight,
          elevation: 5,
          margin: const EdgeInsets.symmetric(vertical: 8.0),
          child: ListTile(
            contentPadding: const EdgeInsets.all(16.0),
            title: Text(
              bookName,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 4.0),
                Text('Chapters: $chapters'),
                Text('Author: $author'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
