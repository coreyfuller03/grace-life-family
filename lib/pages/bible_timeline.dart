import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timeline_tile/timeline_tile.dart';

import '../globals.dart' as globals;
import '../nav.dart';

class BibleTimeline extends StatefulWidget {
  const BibleTimeline({super.key});

  @override
  BibleTimelineState createState() => BibleTimelineState();
}

class BibleTimelineState extends State<BibleTimeline> {
  bool allRevealed = false;
  List<TimelineData> _beginning = [];
  List<TimelineData> _postFlood = [];
  List<TimelineData> _israel = [];
  List<TimelineData> _nt = [];

  @override
  void initState() {
    _beginning = _generateBeginningData();
    _postFlood = _generatePostFloodData();
    _israel = _generateIsraelData();
    _nt = _generateNewTestamentData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        drawer: const NavDrawer(),
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          iconTheme: IconThemeData(
            size: globals.largeFont,
            color: Colors.grey.shade100,
          ),
          title: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              'BIBLE TIMELINE',
              style: TextStyle(
                fontFamily: 'BigNeo',
                fontSize: globals.xLargeFont,
              ),
            ),
          ),
          actions: <Widget>[
            FittedBox(
              fit: BoxFit.scaleDown,
              child: IconButton(
                alignment: Alignment.center,
                color: Colors.grey.shade100,
                iconSize: globals.largeFont,
                icon: const Icon(Icons.help_outline),
                onPressed: () => showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    backgroundColor: Colors.grey.shade100,
                    title: const Text('Bible Timeline'),
                    content: const Text(
                        'Let\'s work on the Bible Timeline! You can scroll through the timeline and quiz yourself or your family. Tap a covered word to reveal the answer, or tap the button at the bottom of the page to show all the answers.'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'OK'),
                        style: TextButton.styleFrom(
                          foregroundColor: Colors.grey.shade900,
                        ),
                        child: const Text('OK'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColorLight,
          ),
          child: SafeArea(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              extendBodyBehindAppBar: true,
              body: Center(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: globals.xSmallPadding),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: CustomScrollView(
                          slivers: <Widget>[
                            SliverPadding(
                                padding: EdgeInsets.all(globals.smallPadding)),
                            SliverList(
                              delegate: SliverChildListDelegate(<Widget>[
                                const MessageTimeline(
                                  message: 'The Beginning',
                                ),
                              ]),
                            ),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SubTimeline(
                                data: _beginning, allRevealed: allRevealed),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SliverList(
                              delegate: SliverChildListDelegate(<Widget>[
                                const MessageTimeline(
                                  message: 'After the flood',
                                ),
                              ]),
                            ),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SubTimeline(
                                data: _postFlood, allRevealed: allRevealed),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SliverList(
                              delegate: SliverChildListDelegate(<Widget>[
                                const MessageTimeline(
                                  message: 'The Nation of Israel',
                                ),
                              ]),
                            ),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SubTimeline(
                                data: _israel, allRevealed: allRevealed),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SliverList(
                              delegate: SliverChildListDelegate(<Widget>[
                                const MessageTimeline(
                                  message: 'The New Testament',
                                ),
                              ]),
                            ),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.xSmallPadding)),
                            SubTimeline(data: _nt, allRevealed: allRevealed),
                            SliverPadding(
                                padding: EdgeInsets.only(
                                    top: globals.mediumPadding)),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            setState(() {
              allRevealed = !allRevealed;
            });
          },
          foregroundColor: Colors.grey.shade100,
          backgroundColor: Theme.of(context).splashColor,
          label: allRevealed
              ? const Icon(Icons.visibility_off_outlined)
              : const Icon(Icons.visibility_outlined),
        ),
      ),
    );
  }

  List<TimelineData> _generateBeginningData() {
    return <TimelineData>[
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.restroom, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'In the beginning God created the heavens and the earth. He created two people by the names of ',
          ' and ',
          '.'
        ],
        answers: [Answer(text: 'ADAM'), Answer(text: 'EVE')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.faceSadCry, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'Did Adam and Eve obey God or disobey God? ',
          '. And the Bible calls that ',
          '.'
        ],
        answers: [Answer(text: 'DISOBEY'), Answer(text: 'SIN')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.handHoldingHeart,
            color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'Now everyone born after Adam and Eve is born into sin, but God stepped into the garden and made them a ',
          '.'
        ],
        answers: [Answer(text: 'PROMISE')],
      ),
      TimelineData(
        icon:
            FaIcon(FontAwesomeIcons.parachuteBox, color: Colors.grey.shade900),
        leftChild: false,
        prompts: ['God promised that He would send a ', ' to defeat sin.'],
        answers: [Answer(text: 'SAVIOR')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.arrowTrendDown,
            color: Colors.grey.shade900),
        leftChild: true,
        prompts: ['But sin just got ', '.'],
        answers: [Answer(text: 'WORSE')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.water, color: Colors.grey.shade900),
        leftChild: false,
        prompts: ['So God sent a ', '.'],
        answers: [Answer(text: 'FLOOD')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.ship, color: Colors.grey.shade900),
        leftChild: true,
        prompts: ['He saved one man by the name of ', ' and his ', '.'],
        answers: [Answer(text: 'NOAH'), Answer(text: 'family')],
      ),
    ];
  }

  List<TimelineData> _generatePostFloodData() {
    return <TimelineData>[
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.gopuram, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'And yet 400 years later the people started building a tower called the Tower of ',
          ' because they wanted to reach God and bring glory to themselves.'
        ],
        answers: [Answer(text: 'BABEL')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.language, color: Colors.grey.shade900),
        leftChild: true,
        prompts: ['So God confused all of their ', '.'],
        answers: [Answer(text: 'LANGUAGES')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.earthAfrica, color: Colors.grey.shade900),
        leftChild: false,
        prompts: ['And that was the beginning of the ', ' of the earth.'],
        answers: [Answer(text: 'NATIONS')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.handPointUp, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'God chose one man to form a new nation and his name was ',
          '.'
        ],
        answers: [Answer(text: 'ABRAHAM')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.gift, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'He promised Abraham 3 things: lots of ',
          ', lots of ',
          ', and a ',
          ' to the nations of the earth.'
        ],
        answers: [
          Answer(text: 'CHILDREN'),
          Answer(text: 'LAND'),
          Answer(text: 'BLESSING')
        ],
      ),
    ];
  }

  List<TimelineData> _generateIsraelData() {
    return <TimelineData>[
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.baby, color: Colors.grey.shade900),
        leftChild: true,
        prompts: ['Abraham had ', ', Isaac had ', ', and Jacob had ', '.'],
        answers: [
          Answer(text: 'ISAAC'),
          Answer(text: 'JACOB'),
          Answer(text: 'JOSEPH')
        ],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.dungeon, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'The first blessing came about and Abraham had so many children and grandchildren so they filled the land of ',
          '. They were ',
          ' to Pharaoh for 400 years.'
        ],
        answers: [Answer(text: 'EGYPT'), Answer(text: 'SLAVES')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.caravan, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'God raised up ',
          ' to bring them out of slavery, but he didn’t take them into the promise land.'
        ],
        answers: [Answer(text: 'MOSES')],
      ),
      TimelineData(
        icon:
            FaIcon(FontAwesomeIcons.personHiking, color: Colors.grey.shade900),
        leftChild: false,
        prompts: ['Who took them into the promise land? '],
        answers: [Answer(text: 'JOSHUA')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.gavel, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'The people’s hearts were still ',
          ', so God would send ',
          ' to rescue the people.'
        ],
        answers: [Answer(text: 'WICKED'), Answer(text: 'JUDGES')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.crown, color: Colors.grey.shade900),
        leftChild: false,
        prompts: ['The people wanted a ', '.'],
        answers: [Answer(text: 'KING')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.users, color: Colors.grey.shade900),
        leftChild: true,
        prompts: ['God gave them King ', ', King ', ', and King ', '.'],
        answers: [
          Answer(text: 'SAUL'),
          Answer(text: 'DAVID'),
          Answer(text: 'SOLOMON')
        ],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.faceSadTear, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'Then the Kingdoms were ',
          '. The Assyrians destroyed the people in the Northern Kingdom. The Babylonians kept people of Israel in exile in the Southern Kingdom for 70 years.'
        ],
        answers: [Answer(text: 'SPLIT')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.hammer, color: Colors.grey.shade900),
        leftChild: true,
        prompts: ['Eventually, the Israelites returned and rebuilt ', '.'],
        answers: [Answer(text: 'JERUSALEM')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.volumeOff, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'For 400 years God didn’t speak to the people, there was just ',
          '.'
        ],
        answers: [Answer(text: 'SILENCE')],
      ),
    ];
  }

  List<TimelineData> _generateNewTestamentData() {
    return <TimelineData>[
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.baby, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'God talked to a girl by the name of ',
          ' and said that the Savior was in her tummy and His name was ',
          '.'
        ],
        answers: [Answer(text: 'MARY'), Answer(text: 'JESUS')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.check, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'Jesus was born and He lived what kind of life?',
          ' and ',
          '.'
        ],
        answers: [Answer(text: 'PERFECT'), Answer(text: 'SINLESS')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.cross, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'Jesus died on the ',
          ', rose from the ',
          ', and ascended into ',
          '.'
        ],
        answers: [
          Answer(text: 'CROSS'),
          Answer(text: 'GRAVE'),
          Answer(text: 'HEAVEN')
        ],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.church, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'Jesus sent us the ',
          ' and that was the beginning of the ',
          '.'
        ],
        answers: [Answer(text: 'HOLY SPIRIT'), Answer(text: 'CHURCH')],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.peopleGroup, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          '2,000 years later, we are doing what Jesus told us to do: making disciples.'
        ],
        answers: [],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.arrowsDownToPeople,
            color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'Jesus will come back for us (the Church) and we call that the ',
          '.'
        ],
        answers: [Answer(text: 'RAPTURE')],
      ),
      TimelineData(
        icon:
            FaIcon(FontAwesomeIcons.earthAmericas, color: Colors.grey.shade900),
        leftChild: false,
        prompts: [
          'There will be 7 years of tribulation. Jesus will come to Earth and throw the devil into jail for 1,000 years as Jesus rules and reigns on Earth.'
        ],
        answers: [],
      ),
      TimelineData(
        icon: FaIcon(FontAwesomeIcons.infinity, color: Colors.grey.shade900),
        leftChild: true,
        prompts: [
          'Then Jesus will finally destroy the devil once and for all. Jesus will make a new ',
          ' and a new ',
          '. We will rule with Jesus forever and ',
          '!'
        ],
        answers: [
          Answer(text: 'HEAVEN'),
          Answer(text: 'EARTH'),
          Answer(text: 'EVER')
        ],
      ),
    ];
  }
}

class SubTimeline extends StatefulWidget {
  final List<TimelineData> data;
  final bool allRevealed;

  const SubTimeline({super.key, required this.data, required this.allRevealed});

  @override
  SubTimelineState createState() => SubTimelineState();
}

class SubTimelineState extends State<SubTimeline> {
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          final TimelineData event = widget.data[index];

          final child = SubTimelineChild(
            prompts: event.prompts,
            answers: event.answers,
            isLeftChild: event.leftChild,
            allRevealed: widget.allRevealed,
          );

          return TimelineTile(
            alignment: TimelineAlign.center,
            endChild: event.leftChild ? null : child,
            startChild: event.leftChild ? child : null,
            indicatorStyle: IndicatorStyle(
              width: 50,
              height: 50,
              indicator: TimelineIndicator(icon: event.icon),
              drawGap: true,
            ),
            beforeLineStyle: LineStyle(
              color: Colors.grey.shade900.withOpacity(0.2),
              thickness: 3,
            ),
          );
        },
        childCount: widget.data.length,
      ),
    );
  }
}

class MessageTimeline extends StatelessWidget {
  const MessageTimeline({super.key, required this.message});

  final String message;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.all(8),
        margin: const EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          color: Colors.grey.shade900.withOpacity(0.2),
          borderRadius: const BorderRadius.all(Radius.circular(8)),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: globals.smallFont,
                    color: Colors.grey.shade900,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Answer {
  final String text;
  bool revealed = false;

  Answer({
    required this.text,
  });
}

class SubTimelineChild extends StatefulWidget {
  final List<String> prompts;
  final List<Answer> answers;
  final bool isLeftChild;
  final bool allRevealed;

  const SubTimelineChild({
    super.key,
    required this.prompts,
    required this.answers,
    required this.isLeftChild,
    required this.allRevealed,
  });

  @override
  SubTimelineChildState createState() => SubTimelineChildState();
}

class SubTimelineChildState extends State<SubTimelineChild> {
  @override
  Widget build(BuildContext context) {
    List<TextSpan> promptAndAnswer = <TextSpan>[];

    for (int i = 0; i < widget.prompts.length; i++) {
      promptAndAnswer.add(
        TextSpan(text: widget.prompts[i]),
      );
      if (widget.answers.length > i) {
        promptAndAnswer.add(
          TextSpan(
            recognizer: TapGestureRecognizer()
              ..onTap = () => setState(() =>
                  widget.answers[i].revealed = !widget.answers[i].revealed),
            text: widget.answers[i].text,
            style: TextStyle(
              fontFamily: 'BigNeo',
              backgroundColor: widget.allRevealed || widget.answers[i].revealed
                  ? Colors.transparent
                  : Colors.grey,
              color: widget.allRevealed || widget.answers[i].revealed
                  ? Theme.of(context).primaryColorDark
                  : Colors.grey,
              fontSize: globals.smallFont,
              height: 1.15,
            ),
          ),
        );
      }
    }

    RichText promtAndAnswerText = RichText(
      textAlign: widget.isLeftChild ? TextAlign.right : TextAlign.left,
      text: TextSpan(
        style: TextStyle(color: Colors.grey.shade900),
        children: promptAndAnswer,
      ),
    );

    return Padding(
      padding: widget.isLeftChild
          ? const EdgeInsets.only(left: 16, top: 10, bottom: 10, right: 10)
          : const EdgeInsets.only(right: 16, top: 10, bottom: 10, left: 10),
      child: Center(
        child: promtAndAnswerText,
      ),
    );
  }
}

class TimelineIndicator extends StatelessWidget {
  const TimelineIndicator({super.key, required this.icon});

  final FaIcon icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey.shade900.withOpacity(0.2),
          width: 3,
        ),
        shape: BoxShape.circle,
      ),
      child: Center(
        child: icon,
      ),
    );
  }
}

class TimelineData {
  TimelineData({
    required this.leftChild,
    required this.prompts,
    required this.answers,
    required this.icon,
  });

  final bool leftChild;
  final List<String> prompts;
  final List<Answer> answers;
  final FaIcon icon;
}
