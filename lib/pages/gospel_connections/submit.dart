import 'dart:math';
import 'dart:typed_data';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../../globals.dart' as globals;
import '../../storage/auth.dart';
import 'age_selection.dart';

const _alphaNumericChars =
    'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';

class NameInputPage extends StatefulWidget {
  final Uint8List? pngBytes;
  final int selectedAge;
  final String gc;

  const NameInputPage(
      {super.key,
      required this.selectedAge,
      required this.gc,
      required this.pngBytes});

  @override
  NameInputPageState createState() => NameInputPageState();
}

class NameInputPageState extends State<NameInputPage> {
  bool connected = true;
  bool submitting = false;
  String _firstName = '';
  String _lastName = '';
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _fNameFocusNode = FocusNode();
  final FocusNode _lNameFocusNode = FocusNode();

  Future<void> _isFirebaseAvailable() async {
    if (globals.firebaseAnonUser == null) {
      return;
    }
    var connectivityResult = await (Connectivity().checkConnectivity());
    bool connectedToInternet =
        ((connectivityResult == ConnectivityResult.mobile &&
                await InternetConnectionChecker().hasConnection) ||
            connectivityResult == ConnectivityResult.wifi &&
                await InternetConnectionChecker().hasConnection ||
            connectivityResult == ConnectivityResult.vpn &&
                await InternetConnectionChecker().hasConnection);
    if (mounted) {
      setState(() {
        connected = connectedToInternet;
      });
    }
  }

  final Random _rnd = Random();
  String _getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length,
      (_) => _alphaNumericChars
          .codeUnitAt(_rnd.nextInt(_alphaNumericChars.length))));

  void _submitGospelConnection() async {
    final navigator = Navigator.of(context);
    setState(() {
      submitting = true;
    });
    if (!connected) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: RichText(
            text: TextSpan(
              children: [
                WidgetSpan(
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Icon(
                      Icons.warning,
                      size: globals.xSmallFont,
                      color: Colors.yellow,
                    ),
                  ),
                ),
                const TextSpan(
                  text: ' Please check your internet connection and try again.',
                ),
              ],
            ),
          ),
          duration: const Duration(seconds: 5),
        ),
      );
      setState(() {
        submitting = false;
      });
      return;
    }

    if (FirebaseAuth.instance.currentUser == null) {
      final authService = AuthService();
      dynamic anonUser = await authService.signInAnon();
      if (anonUser == null) {
        setState(() {
          submitting = false;
        });
        return;
      }
      globals.firebaseAnonUser = anonUser;
    }

    try {
      const bucket = 'gospel_connections';
      DateTime today = DateTime.now();

      String subdirectory = '${today.year}_${today.month}_${today.day}';
      String gcObjectName = 'Gospel_Connection_${_getRandomString(15)}';
      String gospelConnectionText;

      String division;
      if (widget.selectedAge <= 6) {
        division = 'young';
      } else if (widget.selectedAge > 6 && widget.selectedAge <= 11) {
        division = 'middle';
      } else {
        division = 'old';
      }

      if (widget.pngBytes != null) {
        gospelConnectionText =
            '$_firstName $_lastName submitted a drawing on $today.';
        subdirectory += '/drawings/$division/${_firstName}_$_lastName';
        await globals.firebaseStorage!
            .ref()
            .child('$bucket/$subdirectory/IMG_${_getRandomString(15)}.png')
            .putData(widget.pngBytes!);
      } else {
        gospelConnectionText =
            'On $today, $_firstName $_lastName connected the gosepl to: ${widget.gc}';
        subdirectory +=
            '/written_connections/$division/${_firstName}_$_lastName';
      }
      await globals.firebaseStorage!
          .ref()
          .child('$bucket/$subdirectory/$gcObjectName.txt')
          .putString(gospelConnectionText);

      navigator.push(
        MaterialPageRoute(
          builder: (context) => const AgeSelectionPage(submitted: true),
        ),
      );

      setState(() {
        globals.gcDrawingPoints = [];
        globals.gcText = "";
        globals.gcFirstName = "";
        globals.gcLastName = "";
      });
    } catch (error) {
      _buildSnackBar(
          "Error uploading your Gospel connection. Please try again.");
    }
    setState(() {
      submitting = false;
    });
  }

  void _buildSnackBar(String gcErrorMessage) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Icon(
                    Icons.warning,
                    size: globals.xSmallFont,
                    color: Colors.yellow,
                  ),
                ),
              ),
              TextSpan(
                text: ' $gcErrorMessage',
              ),
            ],
          ),
        ),
        duration: const Duration(seconds: 5),
      ),
    );
  }

  @override
  void initState() {
    _firstName = globals.gcFirstName;
    _lastName = globals.gcLastName;
    _isFirebaseAvailable();
    super.initState();
  }

  @override
  void dispose() {
    _fNameFocusNode.dispose();
    _lNameFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool under13 = widget.selectedAge < 13;
    double formWidth = globals.screenWidth * .7;

    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) async {
        if (didPop) {
          return;
        }
        globals.gcFirstName = _firstName;
        globals.gcLastName = _lastName;
        final NavigatorState navigator = Navigator.of(context);
        navigator.pop();
      },
      child: GestureDetector(
        onTap: _dismissKeyboard,
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            iconTheme: IconThemeData(
              size: globals.largeFont,
              color: Colors.grey.shade100,
            ),
            title: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'MY GC',
                style: TextStyle(
                  fontFamily: 'BigNeo',
                  fontSize: globals.xLargeFont,
                ),
              ),
            ),
          ),
          body: LoadingOverlay(
            isLoading: submitting,
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        "ALMOST",
                        style: TextStyle(
                          fontFamily: 'BigNeo',
                          fontSize: globals.superLargeFont,
                        ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        "THERE!",
                        style: TextStyle(
                          fontFamily: 'BigNeo',
                          fontSize: globals.superLargeFont,
                        ),
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: globals.mediumPadding,
                          left: globals.xSmallPadding,
                          right: globals.xSmallPadding,
                          bottom: globals.xLargePadding,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: formWidth,
                              child: TextFormField(
                                initialValue: _firstName,
                                focusNode: _fNameFocusNode,
                                onChanged: (value) {
                                  setState(() {
                                    _firstName = value;
                                  });
                                },
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter the ${widget.selectedAge < 13 ? 'Parent/Guardian First' : 'First'} Name';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    labelText: under13
                                        ? 'Parent/Guardian First Name'
                                        : 'First Name'),
                              ),
                            ),
                            SizedBox(height: globals.xSmallPadding),
                            SizedBox(
                              width: formWidth,
                              child: TextFormField(
                                initialValue: _lastName,
                                focusNode: _lNameFocusNode,
                                onChanged: (value) {
                                  setState(() {
                                    _lastName = value;
                                  });
                                },
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter the ${widget.selectedAge < 13 ? 'Parent/Guardian Last' : 'Last'} Name';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    labelText: under13
                                        ? 'Parent/Guardian Last Name'
                                        : 'Last Name'),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
            foregroundColor: Colors.grey.shade100,
            backgroundColor: submitting
                ? Theme.of(context).disabledColor
                : Theme.of(context).splashColor,
            child: Icon(
              Icons.check,
              size: globals.xLargeFont,
            ),
            onPressed: () {
              _dismissKeyboard();
              if (_formKey.currentState?.validate() ?? false) {
                _submitGospelConnection();
              }
            },
          ),
        ),
      ),
    );
  }

  void _dismissKeyboard() {
    _fNameFocusNode.unfocus();
    _lNameFocusNode.unfocus();
  }
}
