import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../../drawing_area.dart';
import '../../globals.dart' as globals;
import 'submit.dart';

Image? drawnImage;
Uint8List? pngBytes;
List<DrawingArea?> points = [];

class CustomPaintArea extends StatefulWidget {
  const CustomPaintArea({super.key});

  Image? getDrawnImage() {
    return drawnImage;
  }

  @override
  CustomPaintAreaState createState() {
    return CustomPaintAreaState();
  }
}

class CustomPaintAreaState extends State<CustomPaintArea> {
  Color? selectedColor;
  double strokeWidth = 0;
  final GlobalKey _globalKey = GlobalKey();

  Future<void> takeScreenshot(GlobalKey key) async {
    try {
      RenderRepaintBoundary? boundary =
          key.currentContext?.findRenderObject() as RenderRepaintBoundary?;
      if (boundary != null) {
        ui.Image image = await boundary.toImage(pixelRatio: 3.0);

        ByteData? byteData =
            await image.toByteData(format: ui.ImageByteFormat.png);
        if (byteData != null) {
          setState(() {
            pngBytes = byteData.buffer.asUint8List();
            drawnImage = Image.memory(pngBytes!);
          });
          return;
        }
      }
    } catch (e) {
      pngBytes = null;
      drawnImage = null;
      return;
    }

    pngBytes = null;
    drawnImage = null;
    return;
  }

  @override
  void initState() {
    super.initState();
    selectedColor = Colors.black;
    strokeWidth = 2.0;
    points = globals.gcDrawingPoints;
  }

  void selectColor() {
    showDialog(
      context: context,
      builder: (context) {
        return ConstrainedBox(
          constraints: BoxConstraints(maxHeight: globals.screenHeight * .7),
          child: AlertDialog(
            title: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'PICK A COLOR',
                style: TextStyle(
                  fontFamily: 'BigNeo',
                  fontSize: globals.xLargeFont,
                ),
              ),
            ),
            content: SizedBox(
              child: BlockPicker(
                pickerColor: selectedColor!,
                onColorChanged: (color) {
                  setState(() {
                    selectedColor = color;
                  });
                },
              ),
            ),
            actions: <Widget>[
              FittedBox(
                fit: BoxFit.scaleDown,
                child: IconButton(
                  alignment: Alignment.center,
                  color: Theme.of(context).splashColor,
                  iconSize: globals.superLargeFont,
                  icon: const Icon(Icons.check_circle),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(vertical: globals.xSmallPadding),
          child: AppBar(
            iconTheme: IconThemeData(
              size: globals.largeFont,
              color: Colors.grey.shade100,
            ),
            primary: false,
            leadingWidth: 100,
            actions: <Widget>[
              IconButton(
                padding: EdgeInsets.only(
                  left: globals.xxSmallPadding,
                ),
                color: selectedColor,
                icon: Icon(
                  Icons.color_lens,
                  size: globals.screenHeight / 15.0,
                ),
                onPressed: () {
                  selectColor();
                },
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(
                      left: globals.xSmallPadding,
                      right: globals.xSmallPadding),
                  child: Slider(
                    min: 1.0,
                    max: 7.0,
                    activeColor: selectedColor,
                    value: strokeWidth,
                    onChanged: (value) {
                      setState(() {
                        strokeWidth = value;
                      });
                    },
                  ),
                ),
              ),
              IconButton(
                padding: EdgeInsets.only(
                  right: globals.xxSmallPadding,
                ),
                color: Colors.grey.shade900,
                icon: Icon(
                  Icons.layers_clear,
                  size: globals.screenHeight / 15.0,
                ),
                onPressed: () {
                  setState(() {
                    points.clear();
                  });
                },
              ),
            ],
            backgroundColor: Colors.transparent,
            automaticallyImplyLeading: false,
          ),
        ),
        Container(
          width: width * 0.9,
          height: height * 0.65,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(20.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.4),
                blurRadius: 5.0,
                spreadRadius: 1.0,
              )
            ],
          ),
          child: RepaintBoundary(
            key: _globalKey,
            child: GestureDetector(
              onPanDown: (details) {
                setState(() {
                  points.add(
                    DrawingArea(
                      point: details.localPosition,
                      areaPaint: Paint()
                        ..strokeCap = StrokeCap.round
                        ..isAntiAlias = true
                        ..color = selectedColor!
                        ..strokeWidth = strokeWidth,
                    ),
                  );
                });
              },
              onPanUpdate: (details) {
                setState(() {
                  points.add(
                    DrawingArea(
                      point: details.localPosition,
                      areaPaint: Paint()
                        ..strokeCap = StrokeCap.round
                        ..isAntiAlias = true
                        ..color = selectedColor!
                        ..strokeWidth = strokeWidth,
                    ),
                  );
                });
              },
              onPanEnd: (details) {
                setState(() {
                  takeScreenshot(_globalKey);
                  points.add(null);
                });
              },
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(20.0)),
                child: CustomPaint(
                    painter: GospelConnCustomPainter(points: points)),
              ),
            ),
          ),
        ),
        Container(
          decoration: const BoxDecoration(
            color: Colors.blue,
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(138, 35, 135, 1.0),
                Color.fromRGBO(223, 64, 87, 1.0),
                Color.fromRGBO(242, 113, 33, 1.0)
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class GospelConnCustomPainter extends CustomPainter {
  List<DrawingArea?> points = [];

  GospelConnCustomPainter({required this.points});

  @override
  void paint(Canvas canvas, Size size) {
    Paint background = Paint()..color = Colors.white;
    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);

    canvas.drawRect(rect, background);

    for (int x = 0; x < points.length - 1; x++) {
      if (points[x] != null && points[x + 1] != null) {
        Paint paint = points[x]!.areaPaint;
        canvas.drawLine(points[x]!.point, points[x + 1]!.point, paint);
      } else if (points[x] != null && points[x + 1] == null) {
        Paint paint = points[x]!.areaPaint;
        canvas.drawPoints(ui.PointMode.points, [points[x]!.point], paint);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class PaintPage extends StatefulWidget {
  final int selectedAge;

  const PaintPage({super.key, required this.selectedAge});

  @override
  PaintPageState createState() => PaintPageState();
}

class PaintPageState extends State<PaintPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          size: globals.largeFont,
          color: Colors.grey.shade100,
        ),
        title: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            'MY GC',
            style: TextStyle(
              fontFamily: 'BigNeo',
              fontSize: globals.xLargeFont,
            ),
          ),
        ),
      ),
      body: const CustomPaintArea(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.grey.shade100,
        backgroundColor: Theme.of(context).splashColor,
        child: Icon(
          Icons.arrow_forward,
          size: globals.xLargeFont,
        ),
        onPressed: () {
          if (pngBytes != null) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NameInputPage(
                  selectedAge: widget.selectedAge,
                  gc: '',
                  pngBytes: pngBytes,
                ),
              ),
            );
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                dismissDirection: DismissDirection.horizontal,
                duration: const Duration(seconds: 10),
                content: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Icon(
                            Icons.warning,
                            size: globals.xSmallFont,
                            color: Colors.yellow,
                          ),
                        ),
                      ),
                      const TextSpan(
                        text: ' Oops! We are missing your lovely drawing!',
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
