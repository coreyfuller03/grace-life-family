import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../../globals.dart' as globals;
import '../../nav.dart';
import 'paint.dart';
import 'write.dart';

class AgeSelectionPage extends StatefulWidget {
  final bool submitted;

  const AgeSelectionPage({super.key, required this.submitted});

  @override
  AgeSelectionPageState createState() => AgeSelectionPageState();
}

class AgeSelectionPageState extends State<AgeSelectionPage> {
  int selectedAge = 4;
  bool showDrawingOptions = false;
  int? selectedOptionIndex = 0; // 0 for Draw, 1 for Write
  bool connected = true;
  String? ageErrorMessage;
  String? gcErrorMessage;

  Future<void> _isFirebaseAvailable() async {
    //ScaffoldMessenger.of(context).clearSnackBars();
    if (globals.firebaseAnonUser == null) {
      return;
    }
    var connectivityResult = await (Connectivity().checkConnectivity());
    bool connectedToInternet =
        ((connectivityResult == ConnectivityResult.mobile &&
                await InternetConnectionChecker().hasConnection) ||
            connectivityResult == ConnectivityResult.wifi &&
                await InternetConnectionChecker().hasConnection ||
            connectivityResult == ConnectivityResult.vpn &&
                await InternetConnectionChecker().hasConnection);
    if (mounted && !connectedToInternet) {
      setState(() {
        connected = connectedToInternet;
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          dismissDirection: DismissDirection.horizontal,
          duration: const Duration(seconds: 10),
          content: Center(
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Icon(
                          Icons.warning,
                          size: globals.xSmallFont,
                          color: Colors.yellow,
                        ),
                      ),
                    ),
                    TextSpan(
                      text: ' No Internet Connection Detected',
                      style: TextStyle(
                        fontSize: globals.smallFont,
                        color: Colors.grey.shade100,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
  }

  void _buildSnackBar() {
    if (widget.submitted) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          dismissDirection: DismissDirection.horizontal,
          backgroundColor: Theme.of(context).primaryColor,
          content: Row(
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  'Thank you. Great job! ',
                  style: TextStyle(fontSize: globals.smallFont),
                ),
              ),
              Icon(Icons.thumb_up_outlined, color: Colors.grey.shade100),
            ],
          ),
          duration: const Duration(seconds: 10),
        ),
      );
    }
  }

  @override
  void initState() {
    _isFirebaseAvailable();
    WidgetsBinding.instance.addPostFrameCallback((_) => _buildSnackBar());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        iconTheme: IconThemeData(
          size: globals.largeFont,
          color: Colors.grey.shade100,
        ),
        title: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            'My GC',
            style: TextStyle(
              fontFamily: 'BigNeo',
              fontSize: globals.xLargeFont,
            ),
          ),
        ),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: globals.smallPadding,
            vertical: globals.mediumPadding,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  'Let\'s hear about your',
                  style: TextStyle(fontSize: globals.largeFont),
                ),
              ),
              Text(
                'GOSPEL CONNECTION!',
                style: TextStyle(
                  fontFamily: 'BigNeo',
                  fontSize: globals.largeFont,
                ),
              ),
              SizedBox(height: globals.smallPadding),
              Text(
                'A Gospel connection is some way that you have seen the Gospel shown in the world around you.',
                style: TextStyle(fontSize: globals.smallFont),
              ),
              const Spacer(),
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        'How old are you?',
                        style: TextStyle(fontSize: globals.mediumFont),
                      ),
                    ),
                    SizedBox(width: globals.xSmallPadding),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        '$selectedAge',
                        style: TextStyle(
                            fontSize: globals.xLargeFont,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              Slider(
                secondaryActiveColor: Theme.of(context).splashColor,
                activeColor: Theme.of(context).splashColor,
                thumbColor: Theme.of(context).splashColor,
                value: selectedAge.toDouble(),
                min: 4,
                max: 14,
                divisions: 12,
                onChanged: (value) {
                  setState(() {
                    selectedAge = value.round();
                    // Check if the selected age is greater than 6
                    showDrawingOptions = selectedAge > 6;
                  });
                },
              ),
              SizedBox(height: globals.smallPadding),
              Opacity(
                opacity: showDrawingOptions ? 1 : 0,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: ToggleSwitch(
                    minWidth: 150,
                    initialLabelIndex: selectedOptionIndex,
                    cornerRadius: 15.0,
                    activeFgColor: Colors.grey.shade100,
                    inactiveBgColor: Theme.of(context).primaryColorLight,
                    inactiveFgColor: Colors.grey.shade900,
                    labels: const ['Draw', 'Write'],
                    radiusStyle: true,
                    icons: const [
                      FontAwesomeIcons.paintbrush,
                      FontAwesomeIcons.pencil,
                    ],
                    fontSize: globals.smallFont,
                    onToggle: (index) {
                      setState(() {
                        selectedOptionIndex = index;
                      });
                    },
                  ),
                ),
              ),
              const Spacer(),
              const Spacer(),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.grey.shade100,
        backgroundColor: connected
            ? Theme.of(context).splashColor
            : Theme.of(context).disabledColor,
        child: Icon(
          Icons.arrow_forward,
          size: globals.xLargeFont,
        ),
        onPressed: () {
          if (!connected) {
            return;
          }
          // Navigate to the next page based on the user's selection
          if (selectedOptionIndex == 0 || selectedAge <= 6) {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PaintPage(selectedAge: selectedAge)),
            );
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => WritePage(selectedAge: selectedAge)),
            );
          }
        },
      ),
    );
  }
}
