import 'package:flutter/material.dart';

import '../../globals.dart' as globals;
import 'submit.dart';

class WritePage extends StatefulWidget {
  final int selectedAge;

  const WritePage({super.key, required this.selectedAge});

  @override
  WritePageState createState() => WritePageState();
}

class WritePageState extends State<WritePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _textFocusNode = FocusNode();
  String _text = '';

  @override
  void dispose() {
    _textFocusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _text = globals.gcText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) async {
        if (didPop) {
          return;
        }
        globals.gcText = _text;
        final NavigatorState navigator = Navigator.of(context);
        navigator.pop();
      },
      child: GestureDetector(
        onTap: _dismissKeyboard,
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            iconTheme: IconThemeData(
              size: globals.largeFont,
              color: Colors.grey.shade100,
            ),
            title: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                'MY GC',
                style: TextStyle(
                  fontFamily: 'BigNeo',
                  fontSize: globals.xLargeFont,
                ),
              ),
            ),
          ),
          body: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: globals.xSmallPadding,
                  vertical: globals.largePadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: globals.mediumPadding),
                    child: Text(
                      'How have you seen the Gospel shown in the world around you?',
                      style: TextStyle(fontSize: globals.smallFont),
                    ),
                  ),
                  Expanded(
                    child: SizedBox.expand(
                      child: TextFormField(
                        initialValue: _text,
                        focusNode: _textFocusNode,
                        onChanged: (value) {
                          setState(() {
                            _text = value;
                          });
                        },
                        maxLines: 15,
                        maxLength: 3000,
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(
                            fontSize: globals.smallFont,
                          ),
                          hintText: 'I connected the Gospel to...',
                          border: const OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'What is your Gospel connection?';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
            foregroundColor: Colors.grey.shade100,
            backgroundColor: Theme.of(context).splashColor,
            child: Icon(
              Icons.arrow_forward,
              size: globals.xLargeFont,
            ),
            onPressed: () {
              _dismissKeyboard();
              if (_formKey.currentState?.validate() ?? false) {
                // Navigate to the next page to collect user's first and last name
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NameInputPage(
                      selectedAge: widget.selectedAge,
                      gc: _text,
                      pngBytes: null,
                    ),
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  void _dismissKeyboard() {
    _textFocusNode.unfocus();
  }
}
