import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../nav.dart';
import '../globals.dart' as globals;

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        drawer: const NavDrawer(),
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          iconTheme: IconThemeData(
            size: globals.largeFont,
            color: Colors.grey.shade100,
          ),
          title: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              'HOME',
              style: TextStyle(
                fontFamily: 'BigNeo',
                fontSize: globals.xLargeFont,
              ),
            ),
          ),
        ),
        body: const Home(),
      ),
    );
  }
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  final ScrollController _scrollController = ScrollController();
  int _pos = 0;
  late Timer _timer;
  List<String> phrases = [
    '➼ growing as families in our knowledge of who GOD is and what HE has done for us',
    '➼ sharing our gospel connections, so that we can celebrate together',
    '➼ setting our minds on things above, not on things of the earth'
  ];

  @override
  void initState() {
    _timer = Timer.periodic(const Duration(seconds: 5), (Timer t) {
      setState(() {
        _pos = (_pos + 1) % globals.kiddosImages.length;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        decoration: BoxDecoration(
          color: Colors.grey.shade100,
        ),
      ),
      SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: globals.xSmallPadding),
              child: Container(
                constraints: BoxConstraints.expand(
                  width: globals.screenWidth,
                  height: globals.screenHeight / 4,
                ),
                padding: EdgeInsets.only(
                  left: globals.smallPadding,
                  right: globals.smallPadding,
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          '👋 Welcome',
                          style: TextStyle(
                            color: Theme.of(context).primaryColorDark,
                            fontSize: globals.xxLargeFont,
                          ),
                        ),
                      ),
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          'FAMILIES',
                          style: TextStyle(
                            fontFamily: 'BigNeo',
                            color: Theme.of(context).primaryColorDark,
                            fontSize: globals.xLargeFont,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            CarouselSlider(
              items: globals.kiddosImages,
              options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                viewportFraction: .9,
                aspectRatio: 2.0,
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                left: globals.xSmallPadding,
                top: globals.smallPadding,
                bottom: globals.xSmallPadding,
                right: globals.smallPadding,
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(globals.xSmallPadding),
                    child: Text(
                      phrases[0],
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: globals.smallFont,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(globals.xSmallPadding),
                    child: Text(
                      phrases[1],
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: globals.smallFont,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(globals.xSmallPadding),
                    child: Text(
                      phrases[2],
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: globals.smallFont,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(globals.xSmallPadding),
                  ),
                ],
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: globals.smallPadding,
                    vertical: globals.xSmallPadding,
                  ),
                  child: Card(
                    color: Theme.of(context).primaryColorLight,
                    clipBehavior: Clip.antiAlias,
                    child: Column(
                      children: [
                        Image.asset('lib/assets/images/lake.JPEG'),
                        ListTile(
                          title: Text(
                            'Grace Life Kids Ministry',
                            style: TextStyle(
                              fontSize: globals.mediumFont,
                            ),
                          ),
                          subtitle: Text(
                            'A Ministry of GLBC',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: globals.smallFont,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            'Our desire is to come alongside families as they disciple their children. We want to pray with you and support you as you point your children to Jesus.',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                            ),
                          ),
                        ),
                        ButtonBar(
                          alignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                              style: TextButton.styleFrom(
                                foregroundColor: Colors.grey.shade100,
                                backgroundColor: Theme.of(context).primaryColor,
                                minimumSize: Size(globals.screenWidth, 50),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                              ),
                              onPressed: () {
                                launchUrl(Uri.parse(
                                    'https://www.campshadowlake.com'));
                              },
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  'Visit Us Online',
                                  style: TextStyle(
                                    fontSize: globals.smallFont,
                                    color: Colors.grey.shade100,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: globals.smallPadding,
                    vertical: globals.xSmallPadding,
                  ),
                  child: Card(
                    color: Theme.of(context).primaryColorLight,
                    clipBehavior: Clip.antiAlias,
                    child: Column(
                      children: [
                        Image.asset('lib/assets/images/camp.JPEG'),
                        ListTile(
                          title: Text(
                            'Camp Shadow Lake',
                            style: TextStyle(
                              fontSize: globals.mediumFont,
                            ),
                          ),
                          subtitle: Text(
                            'Unforgettable Summers',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: globals.smallFont,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            'Camp Shadow Lake is a day camp open to the community for children who have completed K-6th grades.',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                            ),
                          ),
                        ),
                        ButtonBar(
                          alignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                              style: TextButton.styleFrom(
                                foregroundColor: Colors.grey.shade100,
                                backgroundColor: Theme.of(context).primaryColor,
                                minimumSize: Size(globals.screenWidth, 50),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                              ),
                              onPressed: () {
                                launchUrl(Uri.parse(
                                    'https://www.campshadowlake.com'));
                              },
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  'More About Camp',
                                  style: TextStyle(
                                    fontSize: globals.smallFont,
                                    color: Colors.grey.shade100,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: globals.mediumPadding),
              ],
            ),
          ],
        ),
      )
    ]);
  }
}
