import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import '../cards.dart';
import '../flash_card.dart';
import '../globals.dart' as globals;
import '../nav.dart';

class CatechismsWidget extends StatefulWidget {
  const CatechismsWidget({super.key});

  @override
  CatechismsWidgetState createState() => CatechismsWidgetState();
}

class CatechismsWidgetState extends State<CatechismsWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _loadCatechisms(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          Container flashCardBody;
          if (snapshot.hasData) {
            flashCardBody = Container(
              color: Colors.grey.shade100,
              child: Center(
                child: Cards(
                  type: globals.FlashCardTypes.catechisms,
                  flashCards: snapshot.data,
                ),
              ),
            );
          } else {
            flashCardBody = Container(
              color: Colors.grey.shade100,
              child: Center(
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    'Loading flash cards...',
                    style: TextStyle(
                      fontSize: globals.smallFont,
                    ),
                  ),
                ),
              ),
            );
          }
          return GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Scaffold(
              drawer: const NavDrawer(),
              resizeToAvoidBottomInset: true,
              appBar: AppBar(
                iconTheme: IconThemeData(
                  size: globals.largeFont,
                  color: Colors.grey.shade100,
                ),
                title: Center(
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      '52 IN 52',
                      style: TextStyle(
                        fontFamily: 'BigNeo',
                        fontSize: globals.xLargeFont,
                      ),
                    ),
                  ),
                ),
                actions: <Widget>[
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: IconButton(
                      alignment: Alignment.center,
                      color: Colors.grey.shade100,
                      iconSize: globals.largeFont,
                      icon: const Icon(Icons.help_outline),
                      onPressed: () => showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          backgroundColor: Colors.grey.shade100,
                          title: const Text('52 In 52 Cards'),
                          content: const Text(
                              'The 52 In 52 is our set of 52 questions and answers based in the truth of God\'s Word. You can use these flashcards to help you learn them. Tap the card to flip it over. Swipe right or left to get to a different card.'),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'OK'),
                              style: TextButton.styleFrom(
                                foregroundColor: Colors.grey.shade900,
                              ),
                              child: const Text('OK'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: flashCardBody,
            ),
          );
        });
  }
}

Future<List<FlashCard>> _loadCatechisms(BuildContext context) async {
  if (globals.catechismCards.isNotEmpty) {
    return globals.catechismCards;
  }
  List<FlashCard> catechismCards = [];
  if (globals.catechismQuestions.isEmpty) {
    String catechismQuestionsData =
        await rootBundle.loadString('lib/assets/questions.csv');
    globals.catechismQuestions = const CsvToListConverter(
            csvSettingsDetector:
                FirstOccurrenceSettingsDetector(eols: ['\r\n', '\n']))
        .convert(catechismQuestionsData);
  }
  for (int i = 1; i < globals.catechismQuestions.length; i++) {
    catechismCards.add(
      FlashCard(
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            image: const DecorationImage(
              image: AssetImage("lib/assets/images/catechisms-front.JPEG"),
              fit: BoxFit.fitHeight,
            ),
          ),
          alignment: Alignment.center,
          padding: EdgeInsets.only(
            left: globals.xSmallPadding,
            right: globals.xSmallPadding,
            bottom: globals.smallPadding,
          ),
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(globals.xSmallPadding),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      i.toString(),
                      style: TextStyle(
                        fontFamily: 'BigNeo',
                        color: Colors.grey.shade800,
                        fontSize: globals.mediumFont,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  globals.catechismQuestions[i][0],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: globals.mediumFont,
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            image: const DecorationImage(
              image: AssetImage("lib/assets/images/catechisms-back.JPEG"),
              fit: BoxFit.fitHeight,
            ),
          ),
          padding: EdgeInsets.only(
            left: globals.xSmallPadding,
            right: globals.xSmallPadding,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.all(globals.xSmallPadding),
                    child: Text(
                      i.toString(),
                      style: TextStyle(
                        fontFamily: 'BigNeo',
                        color: Colors.grey.shade100,
                        fontSize: globals.mediumFont,
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  children: <Widget>[
                    Text(
                      globals.catechismQuestions[i][1],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: globals.smallFont,
                        color: Colors.grey.shade100,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: globals.smallPadding)),
                  ],
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: <Widget>[
                      Text(
                        globals.catechismQuestions[i][2],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: globals.mediumFont,
                          color: Colors.grey.shade100,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Colors.grey.shade100,
      ),
    );
  }
  return catechismCards;
}
