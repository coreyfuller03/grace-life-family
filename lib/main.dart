import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'attributes.dart';
import 'firebase_options.dart';
import 'globals.dart' as globals;
import 'pages/home.dart';
import 'storage/auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  _loadLocalKiddoImages();
  _loadLocalAttributesImages();
  _loadLocalTimelineImages();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  globals.firebaseStorage = FirebaseStorage.instance;
  final authService = AuthService();
  dynamic anonUser = await authService.signInAnon();
  if (anonUser != null) {
    globals.firebaseAnonUser = anonUser;
  }

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp(const GLKApp()));
}

void _loadLocalKiddoImages() {
  globals.kiddosImages = [];
  for (int i = 1; i <= 7; i++) {
    globals.kiddosImages.add(Container(
      constraints: BoxConstraints.expand(
        height: globals.screenHeight / 3,
      ),
      padding: EdgeInsets.only(
          left: globals.smallPadding,
          bottom: globals.xSmallPadding,
          right: globals.smallPadding),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("lib/assets/images/kiddos/rotation_$i.JPEG"),
          fit: BoxFit.fitWidth,
        ),
      ),
    ));
  }
}

Future<void> _loadLocalAttributesImages() async {
  String jsonContent =
      await rootBundle.loadString("lib/assets/attributes.json");
  Map<String, dynamic> attributesJSON = jsonDecode(jsonContent);
  Attributes attrs = Attributes.fromJson(attributesJSON);
  List<globals.AttributesCard> attrCards = [];
  for (Attribute attr in attrs.attributes) {
    attrCards.add(
      globals.AttributesCard(
        AssetImage(
          'lib/assets/images/attributes/${attr.name}.${attr.fileExtension}',
        ),
        AssetImage(
          'lib/assets/images/attributes/${attr.name}-verse.${attr.fileExtension}',
        ),
        hexToColor(attr.colorHex),
      ),
    );
  }
  globals.attributesCards = attrCards;
}

void _loadLocalTimelineImages() {
  globals.timelineImage = const AssetImage("lib/assets/images/timeline.JPEG");
}

class GLKApp extends StatelessWidget {
  const GLKApp({super.key});

  @override
  Widget build(BuildContext context) {
    globals.screenWidth = MediaQuery.of(context).size.width;
    globals.screenHeight = MediaQuery.of(context).size.height;

    globals.xxLargePadding = globals.screenHeight * .2;
    globals.xLargePadding = globals.screenHeight * .15;
    globals.largePadding = globals.screenHeight * .1;
    globals.mediumPadding = globals.screenHeight * .07;
    globals.smallPadding = globals.screenHeight * .05;
    globals.xSmallPadding = globals.screenHeight * .025;
    globals.xxSmallPadding = globals.screenHeight * .01;

    globals.superLargeFont = globals.screenWidth * .15;
    globals.xxLargeFont = globals.screenWidth * .1;
    globals.xLargeFont = globals.screenWidth * .09;
    globals.largeFont = globals.screenWidth * .08;
    globals.mediumFont = globals.screenWidth * .06;
    globals.smallFont = globals.screenWidth * .04;
    globals.xSmallFont = globals.screenWidth * .03;

    return MaterialApp(
      color: const Color(0xFF3e83aa),
      debugShowCheckedModeBanner: false,
      title: 'Grace Life Family',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: const Color(0xFF84B143),
          foregroundColor: Colors.grey.shade100,
          centerTitle: true,
        ),
        fontFamily: 'Roboto',
        primaryColor: const Color(0xFF84B143),
        primaryColorDark: const Color(0xFF273514),
        primaryColorLight: const Color(0xFFe6efd9),
        splashColor: const Color(0xFF3e83aa),
        disabledColor: Colors.grey,
        buttonTheme: const ButtonThemeData(
          buttonColor: Color(0xFF5c7b2e),
          textTheme: ButtonTextTheme.primary,
        ),
      ),
      home: const HomePage(),
    );
  }
}

Color hexToColor(String code) {
  return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}
