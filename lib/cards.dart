import 'dart:math';

import 'package:another_transformer_page_view/another_transformer_page_view.dart';
import 'package:flutter/material.dart';

import 'flash_card.dart';
import 'globals.dart' as globals;

TransformerPageView? tpv;

class Cards extends StatefulWidget {
  final List<FlashCard> flashCards;
  final globals.FlashCardTypes type;

  const Cards({super.key, required this.type, required this.flashCards});

  @override
  CardsState createState() => CardsState();
}

class CardsState extends State<Cards> {
  bool front = true;
  int startIndex = 0;

  double cardWidth = globals.screenWidth * .95;

  Widget __buildLayout({
    required Key key,
    required Widget card,
    required Color cardColor,
    info,
  }) {
    return Container(
      key: key,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Card(
        elevation: 10.0,
        color: Color.lerp(cardColor, Colors.grey.shade100, 0.7),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            card,
          ],
        ),
      ),
    );
  }

  _resetQuestion() {
    front = true;
  }

  @override
  void initState() {
    startIndex = 0;
    if (widget.type == globals.FlashCardTypes.attributes) {
      startIndex = globals.attributesPage;
    } else {
      startIndex = globals.catechismsPage;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: globals.screenWidth,
      height: globals.screenHeight,
      child: SizedBox(
        width: cardWidth,
        height: cardWidth,
        child: TransformerPageView(
            curve: Curves.easeInBack,
            loop: true,
            onPageChanged: (details) {
              _resetQuestion();
              if (widget.type == globals.FlashCardTypes.attributes) {
                globals.attributesPage = details!.toInt();
              } else {
                globals.catechismsPage = details!.toInt();
              }
            },
            index: startIndex,
            viewportFraction: 0.8,
            transformer: PageTransformerBuilder(
              builder: (Widget child, TransformInfo info) {
                return Container(
                  color: Colors.transparent,
                  child: Scaffold(
                    backgroundColor: Colors.transparent,
                    body: Center(
                      child: Container(
                        constraints: BoxConstraints.tight(
                          Size.square(cardWidth),
                        ),
                        child: _buildFlipAnimation(info),
                      ),
                    ),
                  ),
                );
              },
            ),
            itemCount: widget.flashCards.length),
      ),
    );
  }

  Widget _buildFront(info) {
    return __buildLayout(
      key: const ValueKey(true),
      cardColor: widget.flashCards[info.index].color,
      card: widget.flashCards[info.index].front,
      info: info,
    );
  }

  Widget _buildRear(info) {
    return __buildLayout(
      key: const ValueKey(false),
      cardColor: widget.flashCards[info.index].color,
      card: widget.flashCards[info.index].back,
      info: info,
    );
  }

  Widget _buildFlipAnimation(info) {
    return GestureDetector(
      onTap: () {
        if (mounted) {
          setState(() => front = !front);
        }
      },
      child: AnimatedSwitcher(
        duration: const Duration(milliseconds: 600),
        transitionBuilder: __transitionBuilder,
        layoutBuilder: (widget, list) => Stack(children: [widget!, ...list]),
        switchInCurve: Curves.easeInBack,
        switchOutCurve: Curves.easeInBack.flipped,
        child: front ? _buildFront(info) : _buildRear(info),
      ),
    );
  }

  Widget __transitionBuilder(Widget widget, Animation<double> animation) {
    final rotateAnim = Tween(begin: pi, end: 0.0).animate(animation);
    return AnimatedBuilder(
      animation: rotateAnim,
      child: widget,
      builder: (context, widget) {
        final isUnder = (ValueKey(front) != widget!.key);
        //var tilt = ((animation.value - 0.5).abs() - 0.5) * 0.003;
        //tilt *= isUnder ? -1.0 : 1.0;
        final value =
            isUnder ? min(rotateAnim.value, pi / 2) : rotateAnim.value;
        return Transform(
          transform: Matrix4.rotationY(value),
          alignment: Alignment.center,
          child: widget,
        );
      },
    );
  }
}
