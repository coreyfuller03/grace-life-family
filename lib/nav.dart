import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';

import 'globals.dart' as globals;
import 'pages/attributes.dart';
import 'pages/bible_timeline.dart';
import 'pages/books.dart';
import 'pages/catechisms.dart';
import 'pages/gospel_connections/age_selection.dart';
import 'pages/home.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).primaryColorLight,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.zero,
            decoration: BoxDecoration(color: Theme.of(context).splashColor),
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("lib/assets/images/menu-icon.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.home_outlined),
            title: const Text('Home'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const HomePage()),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.favorite_outline),
            title: const Text('God Is'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const AttributesWidget()),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.list),
            title: const Text('52 in 52'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const CatechismsWidget()),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.lightbulb_outline),
            title: const Text('Gospel Connections'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        const AgeSelectionPage(submitted: false)),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.library_books),
            title: const Text('Books of the Bible'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Books()),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.timeline_outlined),
            title: const Text('Bible Timeline'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const BibleTimeline()),
              )
            },
          ),
          ListTile(
            leading: const Icon(Icons.download_outlined),
            title: const Text('Download the Timeline'),
            onTap: () async {
              File f = await getImageFileFromAssets();
              GallerySaver.saveImage(f.path).then((path) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomePage()),
                );
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                          backgroundColor: Theme.of(context).primaryColorLight,
                          title: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              "👍 Timeline Downloaded to Photos",
                              style: TextStyle(fontSize: globals.xSmallFont),
                            ),
                          ),
                          content: Image.file(f),
                        ));
              });
            },
          ),
        ],
      ),
    );
  }
}

Future<File> getImageFileFromAssets() async {
  final byteData = await rootBundle.load('lib/assets/images/timeline.JPEG');

  Directory appDocDir = await getApplicationDocumentsDirectory();
  String imagesAppDirectory = appDocDir.path;
  final file = await File('$imagesAppDirectory/images/timeline.JPEG')
      .create(recursive: true);

  await file.writeAsBytes(byteData.buffer
      .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file;
}
