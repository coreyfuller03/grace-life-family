import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Attributes {
  List<Attribute> attributes;
  Attributes({required this.attributes});
  factory Attributes.fromJson(Map<String, dynamic> json) =>
      _$AttributesFromJson(json);
  Map<String, dynamic> toJson() => _$AttributesToJson(this);
}

Attributes _$AttributesFromJson(Map<String, dynamic> json) {
  List<dynamic> attributes = json['attributes'] as List;
  return Attributes(
      attributes: List<Attribute>.from(attributes
          .map((attribute) => Attribute.fromJson(attribute))
          .toList()));
}

Map<String, dynamic> _$AttributesToJson(Attributes instance) =>
    <String, dynamic>{
      'name': instance.attributes,
    };

@JsonSerializable()
class Attribute {
  final String name;
  final String fileExtension;
  final String colorHex;
  Attribute(
      {required this.name,
      required this.fileExtension,
      required this.colorHex});
  factory Attribute.fromJson(Map<String, dynamic> json) =>
      _$AttributeFromJson(json);
  Map<String, dynamic> toJson() => _$AttributeToJson(this);
}

Attribute _$AttributeFromJson(Map<String, dynamic> json) {
  return Attribute(
    name: json['name'] as String,
    fileExtension: json['extension'] as String,
    colorHex: json['color'] as String,
  );
}

Map<String, dynamic> _$AttributeToJson(Attribute instance) => <String, dynamic>{
      'name': instance.name,
      'extension': instance.fileExtension,
      'color': instance.colorHex,
    };

