library glk.globals;

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import 'drawing_area.dart';
import 'flash_card.dart';

double screenWidth = 0.0;
double screenHeight = 0.0;

List<Widget> kiddosImages = [];

User? firebaseAnonUser;
FirebaseStorage? firebaseStorage;

// flash cards info
int catechismsPage = 0;
int attributesPage = 0;

enum FlashCardTypes { catechisms, attributes }

List<FlashCard> attributesOfGodCards = [];
List<FlashCard> catechismCards = [];

List<List<dynamic>> catechismQuestions = [];
List<AttributesCard> attributesCards = [];
ImageProvider? timelineImage;

// Gospel connection info
List<DrawingArea?> gcDrawingPoints = [];
String gcText = "";
String gcFirstName = "";
String gcLastName = "";

double xxLargePadding = 0;
double xLargePadding = 0;
double largePadding = 0;
double mediumPadding = 0;
double smallPadding = 0;
double xSmallPadding = 0;
double xxSmallPadding = 0;

double superLargeFont = 0;
double xxLargeFont = 0;
double xLargeFont = 0;
double largeFont = 0;
double mediumFont = 0;
double smallFont = 0;
double xSmallFont = 0;

class AttributesCard {
  late AssetImage front;
  late AssetImage back;
  late Color color;

  AssetImage? getFront() {
    return front;
  }

  AssetImage? getBack() {
    return back;
  }

  Color? getColor() {
    return color;
  }

  AttributesCard(AssetImage f, AssetImage b, Color c) {
    front = f;
    back = b;
    color = c;
  }
}
