import 'package:flutter/material.dart';

class FlashCard {
  late Widget front;
  late Widget back;
  late Color color;

  Widget? getFront() {
    return front;
  }

  Widget? getBack() {
    return back;
  }

  Color? getColor() {
    return color;
  }

  FlashCard(Widget f, Widget b, Color c) {
    front = f;
    back = b;
    color = c;
  }
}
